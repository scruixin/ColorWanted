﻿
namespace ColorWanted.theme
{
    /// <summary>
    /// 主题类型
    /// </summary>
    public enum ThemeType
    {
        /// <summary>
        /// 深色主题
        /// </summary>
        Dark,
        /// <summary>
        /// 浅色主题
        /// </summary>
        Light,
        /// <summary>
        /// 自定义主题
        /// </summary>
        Custom
    }
}
